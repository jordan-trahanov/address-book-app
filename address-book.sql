CREATE DATABASE  IF NOT EXISTS `address-book-app` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `address-book-app`;
-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: address-book-app
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact_data`
--

DROP TABLE IF EXISTS `contact_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_data` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(100) DEFAULT NULL,
  `contact_company` varchar(255) DEFAULT NULL,
  `contact_address` varchar(150) DEFAULT NULL,
  `contact_phone` varchar(20) DEFAULT NULL,
  `contact_email` varchar(50) DEFAULT NULL,
  `contact_notes` varchar(80) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_data`
--

LOCK TABLES `contact_data` WRITE;
/*!40000 ALTER TABLE `contact_data` DISABLE KEYS */;
INSERT INTO `contact_data` VALUES (17,'Kevin Payne','Edgewire','477 American Junction','0079106947769','kpayne0@nhs.uk','In quis justo. Maecenas aliquam lacus. Morbi quis tortor nulla ultrices aliquet.','2016-10-12 00:08:23','0000-00-00 00:00:00'),(18,'Jennifer Stevens','','444 Beilfuss Parkway','00632204887235','jstevens0@nifty.com','','2016-10-12 00:08:59','2016-10-12 00:16:53'),(19,'George Lee','Shufflebeat','668 Rieder Street','2125306246394','glee1@quantcast.com','In quis justo. Maecenas aliquam lacus. Morbi quis tortor nulla ultrices aliquet.','2016-10-12 00:09:36','0000-00-00 00:00:00'),(20,'Anna Smith','Dazzlesphere','11 Harper Alley','00662169662787','asmith2@goodreads.com','In quis justo. Maecenas aliquam lacus. Morbi quis tortor nulla ultrices aliquet.','2016-10-12 00:10:09','0000-00-00 00:00:00'),(21,'Carol Ford','Realblab','8 Prairie Rose Alley','9775123139115','cford3@yelp.com','In quis justo. Maecenas aliquam lacus. Morbi quis tortor nulla ultrices aliquet.','2016-10-12 00:10:54','0000-00-00 00:00:00'),(22,'Craig Lane','Buzzster','32494 Bartelt Center','2383042603392','clane4@miibeian.gov.cn','','2016-10-12 00:11:36','2016-10-12 00:12:04');
/*!40000 ALTER TABLE `contact_data` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-28 14:13:25
