<?php

// Load DB config file
require_once("include/db.php");

include_once "include/head.html";

?>

<body>

	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="row">
					<div class="col-xs-6">
						<h4 class="text-left">Address book</h4>
					</div>

					<div class="col-xs-6 text-right">
						<a class="btn btn-raised btn-primary header-option-button" href="add-contact.php"><i class="material-icons">&#xE145;</i> Add</a>
					</div>
				</div>

				<br />
				<br />

				<div class="row">

				<?php

				// SELECT all existing employees
				$sql = "SELECT * FROM `contact_data`";
				$result = mysqli_query($conn, $sql);

				if (mysqli_num_rows($result) > 0 ) {  ?>

				    <?php
				    // output data of each employee
				    while($contact = mysqli_fetch_assoc($result)) { ?>

				    	<div class="col-xs-12 col-sm-4 contact-box">

				    	<div class="panel panel-primary">
						  <div class="panel-heading">
						    <h3 class="panel-title"><?=$contact["contact_name"];?></h3>
						  </div>
						  <div class="panel-body">

							<?php if (!empty($contact["contact_company"])) : ?>
						    <p class="company">
						    	<i class="material-icons">&#xE88A;</i>
						    	<?=$contact["contact_company"];?>
						    </p>
							<?php endif; ?>

							<?php if (!empty($contact["contact_address"])) : ?>
							<p class="address">
								<i class="material-icons">&#xE0C8;</i>
								<?=$contact["contact_address"];?>
							</p>
							<?php endif; ?>

							<?php if (!empty($contact["contact_phone"])) : ?>
							<p class="phone">
								<i class="material-icons">&#xE0CD;</i>
								<?=$contact["contact_phone"];?>
							</p>
							<?php endif; ?>

							<?php if (!empty($contact["contact_email"])) : ?>
							<p class="email">
								<i class="material-icons">&#xE0BE;</i>
								<?=$contact["contact_email"];?>
							</p>
							<?php endif; ?>

							<?php if (!empty($contact["contact_notes"])) : ?>
							<p class="note">
								<i class="material-icons">&#xE8CD;</i>
								<?=$contact["contact_notes"];?>
							</p>
							<?php endif; ?>

						  </div>
						  <div class="panel-footer">
						  	<div class="row">
							  	<div class="col-xs-6">
								  	<a href="edit-contact.php?&id=<?=$contact["ID"];?>" class="text-info">
					    				<i class="material-icons">&#xE254;</i>
				    				</a>
							  	</div>
							  	<div class="col-xs-6 text-right">
				    				<a href="delete-contact.php?&id=<?=$contact["ID"];?>" class="text-danger">
					    				<i class="material-icons">&#xE872;</i>
				    				</a>
							  	</div>
						  	</div>
						  </div>
						</div>

				    	<?php /*
				    	<tr>
				    		<td class="manage-contact">
				    			<a href="edit-contact.php?&id=<?=$contact["ID"];?>" class="text-info">
				    				<i class="material-icons">&#xE254;</i>
			    				</a>
				    		</td>
				    		<td class="manage-contact">
								<a href="delete-contact.php?&id=<?=$contact["ID"];?>" class="text-danger">
				    				<i class="material-icons">&#xE872;</i>
			    				</a>
				    		</td>
				    	</tr>
				    	*/ ?>

				    	</div>

				    <?php }

				} else {
				    echo "<h4 class='text-center'><strong>No contacts exist!</strong></h4>";
				}

				// Close the connection to DB
				mysqli_close($conn);
				?>

			</div>
		</div>
	</div>

</body>
</html>