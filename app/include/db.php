<?php

// DB: host, user, pass, name
$servername = "localhost";
$username = "root";
$password = "1234";
$dbname = "address-book-app";

// Create connection
$conn = mysqli_connect( $servername, $username, $password, $dbname );

// If connection to the DB is unsuccessful, stop.
if (!$conn) {
    echo mysqli_connect_error();
}

mysqli_set_charset($conn, "utf8");
