<?php

$ajax = array();
$ajax['error'] = 0;
$ajax['message'] = '';

if ( empty($_POST['contact_name']) ) {

	$ajax['error'] = 1;
	$ajax['message'][] = "Name field is required!";
}

if ( empty($_POST['contact_phone']) ) {

	$ajax['error'] = 1;
	$ajax['message'][] = "Phone field is required!";
}

if ( $ajax['error'] ) {

	echo json_encode($ajax);
	exit;
}

$contact_name = $_POST['contact_name'];
$contact_company = $_POST['contact_company'];
$contact_address = $_POST['contact_address'];
$contact_phone = $_POST['contact_phone'];
$contact_email = $_POST['contact_email'];
$contact_notes = $_POST['contact_notes'];

if ( !$ajax['error'] ) {

	// Load DB config file
	require_once("db.php");

	// INSERT contact data
	$sql = "INSERT INTO `contact_data` (`ID`, `contact_name`, `contact_company`, `contact_address`, `contact_phone`, `contact_email`, `contact_notes`, `reg_date`) VALUES (NULL, '$contact_name', '$contact_company', '$contact_address', '$contact_phone', '$contact_email', '$contact_notes', CURRENT_TIMESTAMP);";
	$insert_query = mysqli_query($conn, $sql);

	// Close the connection to DB
	mysqli_close($conn);

	if ( !$insert_query ) {

		$ajax['message'][] = "Error saving changes to DB.";
		$ajax['snackbar'] = "There was a problem adding this contact!";

		echo json_encode($ajax);
		exit;
	}

	$ajax['redirect'] = 'index.php';
	$ajax['snackbar'] = "Contact added successfully!";

	echo json_encode($ajax);
	exit;
}
