<?php

include_once "include/head.html";

// Load DB config file
require_once "include/db.php";

$row['contact_name'] = '';

if ( isset($_REQUEST['id']) ) {

	// Select contact data
	$sql = "SELECT `contact_name` FROM `contact_data` WHERE `ID` = " . $_REQUEST['id'];
	$select_query = mysqli_query($conn, $sql);
  	$row = mysqli_fetch_array($select_query);

}

?>

<body>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">

				<div class="well bs-component">

					<div class="row">
						<div class="col-xs-6">
							<h4 class="text-left">Delete</h4>
						</div>

						<div class="col-xs-6 text-right">
							<a class="btn btn-raised btn-primary header-option-button" href="index.php"><i class="material-icons">&#xE5C4;</i> Back</a>
						</div>
					</div>

					<hr />

					<div class="row">
						<div class="col-xs-12">

							<h3 class="text-center"><?=$row['contact_name'];?></h3>

							<form action="" method="post" class="contact-form">

								<input id="form-type" type="hidden" name="form-type" value="delete">
								<input type="hidden" name="contactID" value="<?=$_REQUEST['id'];?>">

								<div class="validation-errors"></div>

								<div class="alert alert-dismissible alert-danger">
								  	<button type="button" class="close" data-dismiss="alert">X</button>
								  	<h4>Warning!</h4>
								  	<p>You are about to delete this contact!</p>
								</div>

								<button id="submitForm" type="submit" class="btn btn-raised btn-danger btn-block"><i class="material-icons">&#xE872;</i> Delete contact</button>

							</form>
						</div>
					</div>


				</div>

			</div>
		</div>
	</div>

</body>
</html>
