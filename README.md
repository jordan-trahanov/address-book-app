# Address Book App

A simple address book app.

## Features

* List all contacts
* Add a contact
* Edit a contact
* Delete a contact

## Requirements

* PHP >= 5.6
* Node.js >= 4.5

## Installation

1. Clone the project
2. Import the SQL file named `address-book.sql`
3. Run `yarn install` to install npm packages
4. Build the app using `gulp` or `gulp default`
5. Start Browser-sync using `gulp dev`
6. Access at `http://localhost:4000` by default
